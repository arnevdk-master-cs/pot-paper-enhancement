\documentclass{article}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}

\title{Philosophy of Technology: A Critique on Tom Koch's Article on Ethics,
	Bioethics and Transhumanism}
\author{Arne Van Den Kerchove - \texttt{r0579667} \\
	\texttt{arne.vandenkerchove@student.kuleuven.be}
}

\begin{document}
	\maketitle
	
	\section*{Introduction}
	
	Modern advancements in genetic engineering and biotechnology have given a new boost to the transhumanist ideology. At this very moment, experiments are conducted with technologies like the CRISPR-Cas9 technique, which allow for the extensive analysis and modification of the human genome. These new developments bring us a step closer to the goals of enhancement ideologies, like transhumaism. They also give rise to a new debate on the ethics and desirability of these modifications, similar to the debate started by the eugenics movement. Accordingly, many opponents of genetic enhancement compare these new technologies with the eugenics program of Nazi Germany in the 1930's.
	
	The focus of those who object against bioenhancement lies on the risks these technologies could possibly impose on society. Their views are often conservative and tend to doom thinking. The idea that someone could have the power to change the definition of what is human, frightens most people. It is however questionable whether these objections are rational and outweigh the benefits of individual and societal enhancement.
	
	\section*{Summary}
	
	This paper reviews an article by Tom Koch titled \textit{Enhancing Who? Enhancing What? Ethics,
	Bioethics, and Transhumanism}\cite{koch10}. In this article, the author starts by establishing the common argument and motivation that transhumanists and enhancement bioethicists share to justify the research on genetic enhancement. The argument states that the emerging technologies will soon allow the transhumanist agenda to be realized in an ethically permissible way. The author claims this agenda promotes the betterment of the human species by surpassing the individual limits, as determined by a select group of authorities that decide which traits should be selected for. It is the latter in which the comparison with eugenics starts to surface.

	The transhumanist and enhancement bioethics movements share a common foundation with the futuristic and utopic views that lead to the eugenics movement in the 30's. At its basis lay the assumptions that a mechanistic view can be applied to the individual and the society. The structural similarities between eugenics and these movements are the goal of enhancing mankind by eliminating bad characteristics and promoting the good ones. It is however not trivial that selecting for good characteristics leads to desirable societal outcomes or even which characteristics are good or bad. It is not always clear whether the removal of genes seemingly related to a negative characteristic benefit the gene pool of the society. There is also no guarantee that what the designers of the enhanced evolution decide is undesirable, effectively leads to a lower quality of life.
	
	At the moment, our science and technology is not capable of manipulating these broader, more complex properties of life like intelligence or fitness. According to the author, the transhumanist treats these characteristics as simple, mechanic traits which we can manipulate. This leads to a contradiction. The transhumanist advocates science as the main tool to realize his views, yet his goals and the foundations of his views are wildly unscientific. Allowing the transhumanist agenda to direct our societal evolution and the evolution of our species by means of enhancement would thus involve great risks.
	
	When these views are put into practice, the respect for people of difference would disappear. When we force a certain set of characteristics we consider desirable on a person and deny them the right to have characteristics we do not approve of, we deny the potential of that person as they would have been. We neglect the communal and social aspects of human advancements and focus on the atomistic individual. This failure is what relates the modern enhancement bioethics to the old eugenics paradigm.
	
	\section*{Discussion}
	Tom Koch provides an interesting view on the transhumanist and enhancement bioethics agendas. It is clear that advances in areas promoted by both these movements should be closely monitored and regularly checked against ethical standards, philosophical debate and societal desirability. However, it seems at least a bit premature to brand the entire transhumanist philosophy - and therefore one of the most prominent bases for genetic research - as no more than eugenics.
	
	\subsubsection*{Diversity}
	
	In the conclusion of the article, it surfaces that the argument the author makes, closely resembles the Lucebert-argument\cite{lauwaert19} against enhancement. The Dutch poet Lucebert famously stated in one of his poems that ``Everything of value is fragile". If we would take away the imperfections and fragility of individuals in our society, we would lose a lot of value in the identity of these individuals and do not respect them in their personal lifes. While this sounds like an attractive line of reasoning, there are nevertheless a couple of problems with this argument.
	
	The first and most obvious one is in the reverse of the logical implication made by Lucebert. If his verse were true and everything of value was fragile, it still would not mean that everything fragile also has value. Indeed, the author is correct in stating that by imposing enhancements on certain groups of people of difference, we risk losing their identity, their positive characteristics and their potential societal contributions. That does however not imply that every property that makes a certain group different, is worth cherishing. It is still possible to provide enhanced solutions to the problems that make these groups different. After all, by doing this it would be possible to increase their quality of life significantly. Ask 1000 children that have been born with HIV whether they had rather been born genetically immune to the virus; their answers would probably be the same.
	
	Secondly, it is important to comprehend that the enhancements put forward by the transhumanists are not the only way to solve societal problems. There will always be a form of imperfection, that can be either enhanced or solved by a multitude of other societal measurements. This is in contrast with Tom Koch's statement that it would be better to invest only in initiatives of social support to enhance our society.
	There are indeed problems in our current society that can only be solved by politics and social initiatives, but equally, there are also 
	hurdles we can only jump by embracing the artificial enhancement of our species. Transhumanism aims to provide solutions to the latter. 
	Furthermore, the availability of these solutions does in no way detract the right of diversity of the groups they apply to. This is the key difference between the views of liberal transhumanism and Nazi eugenics.
	
	\subsubsection*{Incrementalism}
	
	Another argument made by the author to compare transhumanist enhancement to eugenics, follows from his observation that the technologies promoted by enhancement scientists are by no means advanced enough to complete their goal. There would thus be no guarantee that, if they pursued their agenda, the outcome would be a desirable one. Enhancement technologies have only been able to make small, incremental improvements. There is for instance no complete understanding of human intelligence and, accordingly, no pill that suddenly gives one hyperintelligence. Yet, the transhumanist ideology aims to get rid of `bad' characteristics and enhance `good' ones, like hyperintelligence, while they do not fully comprehend these characteristics. Hence, labeling properties as `good' and `bad' and acting on that account is very dangerous and has the same disadvantages as eugenics.
		
	It is certainly true that it is not trivial to label more complex characteristics of an individual or a group as desirable or undesirable and that this would involve numerous risks. To do so is however not necessary for the self-direction of human evolution, a central topic of transhumanism. In fact, we have been at the wheel of our own evolutionary ship for a long time, without a transhumanist leader telling us where to steer. Take for instance the example Tom Koch gives in his paper of eyeglasses and laser surgery. He states that this is just a small enhancement, a quick technical fix for one individual that does not make that person a better individual or enhance the entire human species.
	
	This line of reasoning is a logical fallacy. Every time researchers discover a new cure to a human defect and make this available to society, there will be individuals who embrace this cure. Furthermore, the discoveries made by science will be incremental in nature, building on earlier discoveries. One by one, the less `good' individuals will cure their defects and will themselves become a better human. The indivuduals are free to decide what is `good' and `bad' themselves, but  this behavior aggregated over an entire population will, step by step and almost unnoticably, shift the average `goodness' of the population in the positive direction. This is the real self-direction of human evolution and what humanity has been doing for a long time. We do not completely understand the process of aging and nobody has invented a cure for old age, yet we live longer than ever before and there has been no need for a transhumanist technocrat telling the world to become older.
	
	This continuous, incremental enhancement of society fits perfectly in the transhumanist ideology. Every single goal, from disease immunity to a posthuman status, can be reached by this process. Enhancement should not be seen as the act of starting to discard undesirable individuals out of the blue or trying to take a giant leap to a posthuman characteristic like immortality, but as a process of repeatedly engineering a solution to the next emerging problem in society. This is what humanity has always done and at which it has become very good. This view on transhumanism has none of the pitfalls identified by Tom Koch that link it to eugenics. Directed evolution needs no grand designer, no authority telling us which individuals are desirable and undesirable. The fact that not every human characteristic we aim to enhance is fully scientifically understood, can even be an an advantage. This way, we have no choice but to direct our research to correcting small, undesirable defects of an individual instead of the more risky operation of trying to make that person a so called good person all at once.
	
	
	\section*{Conclusion}
	In my opinion, Tom Koch's concerns with the transhumanist ideology are reasonable, yet not very realistic. Our future, enhanced evolution - should we embrace transhumanism - could indeed be directed by a group of powerful technocrats that decide who is desirable and who not. Our future evolution could also be directed by the choices we make and our persistence in trying to come up with technological and scientific innovations to solve specific problems of humanity. The latter is not eugenics. It preserves individual and group rights to diversity. It has no dictator telling us that everyone should have blond hair. It does not try to change a bad individual to a good individual by standards we do not comprehend.
	
	What is certain, is that we have been steering our own evolution already. This seems to have happend by small, incremental and individually insignificant steps. Considering these steps combined in the humanity we are today, it becomes clear that we are already enhanced. We are already transhuman and it has not happend by pursuing a eugenic agenda.


	\begin{thebibliography}{9}
		\bibitem{lauwaert19}
		Lauwaert, L. Technology and Enhancement. \textit{Philosophy of Technology}. Leuven, 2019.
		
		\bibitem{koch10}
		Koch, T. Enhancing Who? Enhancing What? Ethics, Bioethics, and Transhumanism. \textit{The Journal of Medicine and Philosophy: A Forum for Bioethics and Philosophy of Medicine}, Volume 35, Issue 6, 1 December 2010, Pages 685–699, \url{https://doi.org/10.1093/jmp/jhq051}.
	\end{thebibliography}



\end{document}

